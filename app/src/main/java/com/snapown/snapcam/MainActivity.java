package com.snapown.snapcam;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

	ImageView image;
	Activity context;
	Preview preview;
	Camera camera;

	ImageView fotoButton;
	LinearLayout progressLayout;
	String path = "/sdcard/SnapOwn/cache/images/";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		context=this;
		
		fotoButton = (ImageView) findViewById(R.id.imageView_foto);

		image = (ImageView) findViewById(R.id.imageView_photo);
		progressLayout = (LinearLayout) findViewById(R.id.progress_layout);
		
		preview = new Preview(this,
				(SurfaceView) findViewById(R.id.surface_view));
		FrameLayout frame = (FrameLayout) findViewById(R.id.preview);
		frame.addView(preview);
		preview.setKeepScreenOn(true);
		fotoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					takePicture();
				} catch (Exception e) {

				}

				fotoButton.setClickable(false);
				progressLayout.setVisibility(View.VISIBLE);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		// TODO Auto-generated method stub
		if(camera==null){
		camera = Camera.open();
		camera.startPreview();
		camera.setErrorCallback(new ErrorCallback() {
			public void onError(int error, Camera mcamera) {

				camera.release();
				camera = Camera.open();
				Log.d("Camera died", "error camera");

			}
		});
		}
		if (camera != null) {
			if (Build.VERSION.SDK_INT >= 14)
				setCameraDisplayOrientation(context,
						CameraInfo.CAMERA_FACING_BACK, camera);
			preview.setCamera(camera);
		}
	}
	
	private void setCameraDisplayOrientation(Activity activity, int cameraId,
			Camera camera) {
		CameraInfo info = new CameraInfo();
		Camera.getCameraInfo(cameraId, info);
		int rotation = activity.getWindowManager().getDefaultDisplay()
				.getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;
		case Surface.ROTATION_90:
			degrees = 90;
			break;
		case Surface.ROTATION_180:
			degrees = 180;
			break;
		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}

		int result;
		if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360; // compensate the mirror
		} else { // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}
		camera.setDisplayOrientation(result);
	}


	
	Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {
		@Override
		public void onAutoFocus(boolean success, Camera camera) {
				
			try{
			camera.takePicture(mShutterCallback, null, jpegCallback);
			}catch(Exception e){
				
			}

		}
	};

	ShutterCallback mShutterCallback = new ShutterCallback() {
		
		@Override
		public void onShutter() {
			// TODO Auto-generated method stub
			
		}
	};
	public void takePicture() {
		camera.autoFocus(mAutoFocusCallback);

	}



	PictureCallback jpegCallback = new PictureCallback() {
		@SuppressWarnings("deprecation")
		public void onPictureTaken(byte[] data, Camera camera) {

			FileOutputStream outStream = null;
			Calendar c = Calendar.getInstance();
			File Directory = new File(path);

			if (!Directory.exists()) {
				Directory.mkdirs();
			}

			try {
				// Write to SD Card
				outStream = new FileOutputStream(path + c.getTime().getSeconds() + ".jpg");
				outStream.write(data);
				outStream.close();


			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {

			}
			
			
			Bitmap realImage;
			 final BitmapFactory.Options options = new BitmapFactory.Options();
			  options.inSampleSize = 5;// to create small image preview we need to lessen the size of orginal image by 1/5th.



			realImage = BitmapFactory.decodeByteArray(data,0,data.length, options);



		
			image.setImageBitmap(realImage);

			fotoButton.setClickable(true);
			image.setClickable(true);
			camera.startPreview();
			progressLayout.setVisibility(View.GONE);//

		}
	};


}
